package io.sevenbit.crdt.demo.util;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;


public class StoreInitializerTest {



    @Test
    public void simpleInitializerTest() {
        for(var size = 2; size < 20; size++) {
            var simpleInit = StoreInitializer.initEmptyStore(1, size);

            Assert.assertEquals(size + 2, simpleInit.size());
            Assert.assertNull(simpleInit.lowerKey(BigDecimal.ZERO));
            Assert.assertNull(simpleInit.higherKey(BigDecimal.ONE));
        }
    }

    @Test
    public void parallelInitializerTest() {
        var size = 20;
        var simpleInit = StoreInitializer.initEmptyStore(1, size);
        var parallelInit = StoreInitializer.initEmptyStoreParallel(1, size);

        Assert.assertEquals(size + 2, simpleInit.size());
        Assert.assertEquals(size + 2, parallelInit.size());

        for(var key : simpleInit.keySet()) {
            Assert.assertNotNull(parallelInit.get(key));
        }
    }

    @Test
    public void size_10M_Test() {
        var size = 10_000_000;
        var simpleInit = StoreInitializer.initEmptyStore(1, size);
        var parallelInit = StoreInitializer.initEmptyStoreParallel(1, size);
    }


}
