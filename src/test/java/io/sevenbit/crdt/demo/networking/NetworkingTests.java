package io.sevenbit.crdt.demo.networking;

import io.sevenbit.crdt.demo.Context;
import io.sevenbit.crdt.demo.TestUtils;
import org.junit.Test;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static io.sevenbit.crdt.demo.TestUtils.waitUntilCondition;

public class NetworkingTests {

    /**
     * Check start and stop commands work with single node
     */
    @Test(expected = Test.None.class /* no exception expected */)
    public void startAndStopSingleNodeTest() throws InterruptedException, ExecutionException, TimeoutException {
        var context = Context.defaultContext();

        var future = Executors.newSingleThreadExecutor().submit(() -> {
            context.start();
            var node = context.addNode();
            waitUntilCondition("wait until node1 complete init", () -> node.state() == Node.State.RUNNING);

            context.stop();
            waitUntilCondition("wait until node1 stopped", () -> node.state() == Node.State.STOPPED);
        });

        future.get(10, TimeUnit.SECONDS);
    }

    @Test(expected = Test.None.class /* no exception expected */)
    public void startAndStopTwoNodesTest() throws InterruptedException, ExecutionException, TimeoutException {
        var context = Context.defaultContext();

        var future = Executors.newSingleThreadExecutor().submit(() -> {
            context.start();
            var node1 = context.addNode();
            var node2 = context.addNode();

            waitUntilCondition("wait until node1 complete init", () -> node1.state() == Node.State.RUNNING);
            waitUntilCondition("wait until node2 complete init", () -> node2.state() == Node.State.RUNNING);

            context.stop();


            waitUntilCondition("wait until node2 stopped", () -> node2.state() == Node.State.STOPPED);

        });

        var result = future.get(15, TimeUnit.SECONDS);
    }


    @Test(expected = Test.None.class /* no exception expected */)
    public void addAndRemove10Nodes() throws InterruptedException, ExecutionException, TimeoutException {
        var context = Context.defaultContext();

        var future = Executors.newSingleThreadExecutor().submit(() -> {
            context.start();
            for(int i = 0; i < 10; i++) {
                context.addNode();
            }
            TestUtils.sleep(10, TimeUnit.SECONDS);

            context.stop();
        });

        future.get(30, TimeUnit.SECONDS);
    }
}
