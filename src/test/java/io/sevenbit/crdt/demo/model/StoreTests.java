package io.sevenbit.crdt.demo.model;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class StoreTests {

    @Test
    public void sharedStateIsSorted() {
        //arrange
        var size = 10;
        var store = Store.fromScratch(1, size);
        //act
        var sortedItems = store.shareInitialState();

        //check collection is sorted
        Assert.assertFalse(sortedItems.isEmpty());
        Item prev = null;
        for (var item : sortedItems) {
            if (prev != null) {
                int cmp = item.index.compareTo(prev.index);
                Assert.assertEquals(1, cmp);
            }
            prev = item;
        }
    }

    @Test
    public void randomBigDecimalTest() {
        var min = BigDecimal.ZERO;
        var max = BigDecimal.ONE;
        for (int i = 0; i < 10; i++) {
            //act
            var value = Store.randomBigDecimal(min, max);
            //assert
            Assert.assertEquals(1, value.compareTo(min));
            Assert.assertEquals(-1, value.compareTo(max));
        }

    }

    @Test
    public void stateSharingTest() {
        //arrange
        var size = 10;
        var store1 = Store.fromScratch(1, size);
        var store2 = Store.fromExistingStore(2, size);
        //act
        store2.loadInitialState(store1.shareInitialState());
        //assert
        assertStoresEqual(store1, store2);
    }

    @Test
    public void applyOperationsInTheSameOrder() {
        //arrange
        var size = 4;
        var store1 = Store.fromScratch(1, size);
        var store2 = Store.fromExistingStore(2, size);
        store2.loadInitialState(store1.shareInitialState());

        var updateOperation = store1.operation(OperationType.UPDATE);
        var removeOperation = store2.operation(OperationType.REMOVE);

        //act
        store1.applyOperation(updateOperation);
        store1.applyOperation(removeOperation);

        store2.applyOperation(updateOperation);
        store2.applyOperation(removeOperation);

        //assert
        assertStoresEqual(store1, store2);
    }

    @Test
    public void applyOperationsInTheDifferentOrder() {
        //arrange
        var size = 4;
        var store1 = Store.fromScratch(1, size);
        var store2 = Store.fromExistingStore(2, size);
        store2.loadInitialState(store1.shareInitialState());

        var updateOperation = store1.operation(OperationType.UPDATE);
        var removeOperation = store2.operation(OperationType.REMOVE);

        //act
        store1.applyOperation(updateOperation);
        store1.applyOperation(removeOperation);

        store2.applyOperation(removeOperation);
        store2.applyOperation(updateOperation);

        //assert
        assertStoresEqual(store1, store2);
    }

    public void applyManyOperationsTwoTheeNodes() {

        var size = 10;
        var operationCount = 10;
        var store1 = Store.fromScratch(1, size);
        var store2 = Store.fromExistingStore(2, size);
        var store3 = Store.fromExistingStore(3, size);

        store2.loadInitialState(store1.shareInitialState());
        store3.loadInitialState(store2.shareInitialState());

        for(int i = 0; i < 10; i++) {
            var op1 = store1.randomOperation();
            var op2 = store2.randomOperation();
            var op3 = store3.randomOperation();


        }





    }

    static void assertStoresEqual(Store store1, Store store2) {
        Assert.assertArrayEquals(store1.shareInitialState().toArray(), store2.shareInitialState().toArray());
    }
}
