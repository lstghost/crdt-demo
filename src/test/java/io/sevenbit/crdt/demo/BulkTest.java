package io.sevenbit.crdt.demo;

import io.sevenbit.crdt.demo.networking.Node;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static io.sevenbit.crdt.demo.TestUtils.waitUntilCondition;

public class BulkTest {

    @Test
    public void run_20_nodes_with_10M_item_store_test() throws InterruptedException, ExecutionException, TimeoutException {
        var context = Context.withStoreSize(1_000_000);
        var nodesCount = 10;
        var secondsToWork = 10;

        var future = Executors.newSingleThreadExecutor().submit(() -> {
            bulkTest(context, nodesCount, secondsToWork);
        });

        future.get();
    }

    private void bulkTest(Context context, int nodesCount, int secondsWork) {
        context.start();
        List<Node> nodes = new ArrayList<>(nodesCount);
        for(int i = 0; i < nodesCount; i++) {
            var node = context.addNode();
            nodes.add(node);
            TestUtils.sleep(1, TimeUnit.SECONDS);
        }
        for(var node : nodes) {
            waitUntilCondition("wait until node " + node.nodeId + " complete init", () -> node.state() == Node.State.RUNNING);
        }

        System.out.println("all nodes initialized");
        TestUtils.sleep(secondsWork, TimeUnit.SECONDS);
        System.out.println(secondsWork + " seconds of work ended");

        context.stop();

        TestUtils.sleep(1, TimeUnit.SECONDS);

        for(var node : nodes) {
        waitUntilCondition("wait until node " + node.nodeId + " stopped", () -> node.state() == Node.State.STOPPED);
        }

        var firstNode = nodes.get(0);
        System.out.println("operations made: " + firstNode.nodeMonitor.operationsTotalMonitor.get());
        for(var node : nodes) {
            if(node.nodeId == firstNode.nodeId) continue;
            var isSameState = firstNode.isSameStoreState(node);
            if(isSameState) {
                System.out.println(node.nodeId + ": state is valid");
            }
        }
    }
}
