package io.sevenbit.crdt.demo;

import java.util.concurrent.TimeUnit;
import java.util.function.BooleanSupplier;

public class TestUtils {

    public static void sleep(int value, TimeUnit unit) {
        var millis = unit.toMillis(value);
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Checks condition until will satisfy or max attempt count reached
     * @param condition
     */
    public static boolean waitUntilCondition(BooleanSupplier condition) {
        return waitUntilCondition(null, condition);
    }

    public static boolean waitUntilCondition(String message, BooleanSupplier condition) {
        return waitUntilCondition(message, false, condition);
    }

    public static boolean waitUntilConditionIgnoreExceptions(BooleanSupplier condition) {
        return waitUntilConditionIgnoreExceptions(null, condition);
    }

    public static boolean waitUntilConditionIgnoreExceptions(String message, BooleanSupplier condition) {
        return waitUntilCondition(message, true, condition);
    }

    public static boolean waitUntilCondition(String message, boolean treatExceptionAsWaitMore, BooleanSupplier condition) {
        long started = System.currentTimeMillis();
        boolean met = false;
        for(int i = 0; i < 100; i++) {
            boolean ok;
            try
            {
                ok = condition.getAsBoolean();
            } catch (RuntimeException e)
            {
                if (!treatExceptionAsWaitMore)
                    throw e;
                ok = false;
            }
            if(ok) {
                met = true;
                break;
            } else{
                sleep(100, TimeUnit.MILLISECONDS);
            }
        }
        long elapsed = System.currentTimeMillis() - started;
        if(message != null) {
            System.out.println(message + " spent " + elapsed + "ms waiting, condition met: " + met);
        } else {
            System.out.println("spent " + elapsed + "ms waiting, condition met: " + met);
        }
        return met;
    }


}
