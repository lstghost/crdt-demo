package io.sevenbit.crdt.demo;

public class Config {
    public final App app = new App();
    public final Node node = new Node();


    public static class App {
        public final int MAX_NODES_COUNT = 20;
        public final boolean CASUAL_BRODCASTING = true;
    }

    public static class Node {
        public int DEFAULT_STORE_SIZE = 10_000_000;
        public int NODE_OPERATIONS_PER_SECOND = 5;
        public int QUEUE_CAPACITY = 100;
    }
}
