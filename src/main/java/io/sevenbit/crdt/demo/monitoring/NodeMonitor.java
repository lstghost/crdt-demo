package io.sevenbit.crdt.demo.monitoring;

import java.util.concurrent.atomic.AtomicLong;

public class NodeMonitor {
    public final AtomicLong operationsTotalMonitor = new AtomicLong(0);

    public void operation() {
        operationsTotalMonitor.incrementAndGet();
    }
}
