package io.sevenbit.crdt.demo.model;

import java.util.Random;

public enum OperationType {
    ADD,
    UPDATE,
    REMOVE;
}
