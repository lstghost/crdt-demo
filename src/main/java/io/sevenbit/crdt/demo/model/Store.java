package io.sevenbit.crdt.demo.model;

import io.sevenbit.crdt.demo.util.SynchronizedBigInteger;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;

import static io.sevenbit.crdt.demo.util.StoreInitializer.initEmptyStore;
import static io.sevenbit.crdt.demo.util.StoreInitializer.initEmptyStoreParallel;

/**
 * Items are sorted by Item.index key.
 * Index has a value between 0 and zero
 */
public class Store implements IStore {
    private final int id;
    private final NavigableMap<BigDecimal, Item> data;
    private final Map<BigDecimal, Tombstone> tombstones = new ConcurrentHashMap<>();
    private final static int seed = 123;
    private final int maxSize;
    private final Random r = new Random(seed);
    /**
     * Lamport clock
     */
    private final SynchronizedBigInteger clock = new SynchronizedBigInteger();

    public static Store fromScratch(int id, int storeSize) {
        return new Store(id, storeSize, true);
    }

    public static Store fromExistingStore(int id, int storeSize) {
        return new Store(id, storeSize, false);
    }

    private Store(int id, int storeSize, boolean isFirstNode) {
        this.id = id;
        this.maxSize = storeSize;

        if (isFirstNode) {
            data = storeSize < 100
                    ? initEmptyStore(id, storeSize)
                    : initEmptyStoreParallel(id, storeSize);
        } else {
            this.data = new ConcurrentSkipListMap<>(); //store state will be requested
        }
    }


    @Override
    public synchronized Operation randomOperation() {
        while (true) {
            var operationType = randomOperationType(r);
            var operation = operation(operationType);
            if (operation != null) return operation;
        }
    }


    public Operation operation(OperationType operationType) {
        var randomIndex = randomIndex();

        switch (operationType) {
            case ADD:
                if (isFull()) return null;
                var value = r.nextInt();
                var newIndex = generateNewIndex(randomIndex);
                return new Operation(operationType, new Item(newIndex, value, id, clock.incrementAndGet()));
            case REMOVE:
                if (isEmpty()) return null;
                //get random value between 0 and 1
                var item = data.get(randomIndex);

            case UPDATE:
                item = data.get(randomIndex);
                var newValue = r.nextInt();
                return new Operation(operationType, new Item(item.index, newValue, id, clock.incrementAndGet()));
        }
        throw new RuntimeException("never happen");
    }

    private static OperationType randomOperationType(Random r) {
        var operationTypeIdx = Math.abs((r.nextInt() % OperationType.values().length));
        return OperationType.values()[operationTypeIdx];
    }

    /**
     * Create an index value for new item: (prev + next) / 2
     *
     * @param r
     * @return
     */
    private static final BigDecimal two = BigDecimal.valueOf(2);

    private BigDecimal generateNewIndex(BigDecimal randomIndex) {
        var prevIndex = randomIndex;
        var nextIndex = data.higherKey(prevIndex);
        return (prevIndex.add(nextIndex)).divide(two);
    }

    private BigDecimal randomIndex() {
        var randomBigDecimal = randomBigDecimal(BigDecimal.ZERO, BigDecimal.ONE);
        var result = data.lowerKey(randomBigDecimal);
        if (result == null) {
            return data.higherKey(BigDecimal.ZERO);
        } else {
            return result;
        }
    }

    @Override
    public synchronized void applyOperation(Operation operation) {

        var newTimestamp = clock.setValueIfGreaterAndIncrement(operation.item.timestamp);
        var index = operation.item.index;

        var copy = operation.item.copy();
        copy.timestamp = newTimestamp;

        switch (operation.type) {
            case ADD:
            case UPDATE:
                if (data.containsKey(index)) {
                    var existing = data.get(index);
                    var cmp = existing.timestamp.compareTo(newTimestamp);

                    if (cmp < 0 || (cmp == 0 && existing.nodeId < copy.nodeId)) {
                        data.put(index, copy);
                    }
                } else {
                    if (tombstones.containsKey(index)) {
                        var tombstone = tombstones.get(index);
                        var cmp = tombstone.timestamp.compareTo(newTimestamp);

                        if (cmp < 0 || (cmp == 0 && tombstone.nodeId < copy.nodeId)) {
                            data.put(index, copy);
                            tombstones.remove(index);
                        }

                    } else {
                        data.put(index, copy);
                    }
                }
                break;

            case REMOVE:
                if (data.containsKey(index)) {
                    var existing = data.get(index);
                    var cmp = existing.timestamp.compareTo(newTimestamp);
                    if (cmp < 0 || (cmp == 0 && existing.nodeId < copy.nodeId)) {
                        data.remove(index);
                    }
                }
                if (tombstones.containsKey(index)) {
                    var tombstone = tombstones.get(index);
                    var cmp = tombstone.timestamp.compareTo(newTimestamp);

                    if (cmp < 0 || (cmp == 0 && tombstone.nodeId < copy.nodeId)) {
                        tombstones.put(index, new Tombstone(copy.nodeId, newTimestamp));
                    }

                } else {
                    tombstones.put(index, new Tombstone(copy.nodeId, newTimestamp));
                }
                break;
        }

    }

    @Override
    public synchronized void loadInitialState(Collection<Item> initialState) {
        long start = System.currentTimeMillis();
        if (!data.isEmpty()) throw new RuntimeException("data is already initialized");
        for (var item : initialState) {
            var copy = item.copy();
            data.put(copy.index, copy);
        }
        long elapsed = System.currentTimeMillis() - start;
        System.out.println(id + " load state (size=" + initialState.size() + ") elapsed: " + elapsed);

    }

    @Override
    public Collection<Item> shareInitialState() {
        return data.values();
    }

    @Override
    public synchronized boolean isSameState(IStore other) {
        var otherState = other.shareInitialState();
        for(var otherItem : otherState) {
            var item = data.get(otherItem.index);
            if(item == null) {
                System.out.println("index " + otherItem.index +  " is not present in node " + id);
                return false;
            }
            if(item.value != otherItem.value) {
                System.out.println("index " + item.index + "values are not the same: " + item.value + " != " + otherItem.value);
                return false;
            }
        }
        return true;
    }

    private boolean isFull() {
        return data.size() == maxSize;
    }

    private boolean isEmpty() {
        return data.size() == 0;
    }

    static BigDecimal randomBigDecimal(BigDecimal min, BigDecimal max) {
        BigDecimal randomBigDecimal = min.add(new BigDecimal(Math.random()).multiply(max.subtract(min)));
        return randomBigDecimal.setScale(2, RoundingMode.HALF_UP);
    }


    class Tombstone {
        final BigInteger timestamp;
        final int nodeId;

        Tombstone(int nodeId, BigInteger timestamp) {
            this.timestamp = timestamp;
            this.nodeId = nodeId;
        }
    }


}
