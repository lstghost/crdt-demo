package io.sevenbit.crdt.demo.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Objects;

/**
 * Single element from Store
 */
public class Item {
    final int value;
    final int nodeId; // current value author
    volatile BigInteger timestamp; // Lamport clock timestamp
    /**
     * Index is a value between 0 and 1
     */
    final BigDecimal index;

    public Item(BigDecimal index, int value, int nodeId, BigInteger timestamp) {
        this.index = index;
        this.value = value;
        this.nodeId = nodeId;
        this.timestamp = timestamp;
    }

    public Item copy() {
        var indexCopy = this.index.add(BigDecimal.ZERO);
        return new Item(indexCopy, this.value, this.nodeId, this.timestamp.add(BigInteger.ZERO));
    }

    public static Item LEFT_BORDER = new Item(BigDecimal.ZERO, 0, 0, BigInteger.ZERO);
    public static Item RIGHT_BORDER = new Item(BigDecimal.ONE, 0, 0, BigInteger.ZERO);

    @Override
    public String toString() {
        return "Item{" +
                "value=" + value +
                ", timestamp=" + timestamp +
                ", nodeId=" + nodeId +
                ", index=" + index +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return value == item.value &&
                nodeId == item.nodeId &&
                Objects.equals(timestamp, item.timestamp) &&
                Objects.equals(index, item.index);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, nodeId, timestamp, index);
    }
}
