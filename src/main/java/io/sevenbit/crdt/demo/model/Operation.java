package io.sevenbit.crdt.demo.model;


public class Operation {
    public final OperationType type;
    public final Item item;


    public Operation(OperationType type, Item item) {
        this.type = type;
        this.item = item;
    }

    @Override
    public String toString() {
        return "{" +
                "type=" + type +
                ", item=" + item +
                '}';
    }
}
