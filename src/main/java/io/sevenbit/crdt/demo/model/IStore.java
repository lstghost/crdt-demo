package io.sevenbit.crdt.demo.model;

import java.util.Collection;

public interface IStore {
    Operation randomOperation();
    Operation operation(OperationType operationType);
    void applyOperation(Operation operation);

    void loadInitialState(Collection<Item> initialState);
    Collection<Item> shareInitialState();

    boolean isSameState(IStore other);

}
