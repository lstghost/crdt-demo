package io.sevenbit.crdt.demo;

import io.sevenbit.crdt.demo.networking.Broadcaster;
import io.sevenbit.crdt.demo.networking.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Context {
    private final Broadcaster broadcaster;
    private final ExecutorService executor;
    private final Config config;
    private int nodeIdSequence = 0;
    private List<Integer> activeNodes = new ArrayList<>();

    public Context(Config config) {
        this.config = config;
        this.broadcaster = new Broadcaster(config);
        this.executor = Executors.newFixedThreadPool(1 + 2 * config.app.MAX_NODES_COUNT);
    }

    public static Context defaultContext() {
        return new Context(new Config());
    }

    public static Context withStoreSize(int storeSize) {
        var config = new Config();
        config.node.DEFAULT_STORE_SIZE = storeSize;
        return new Context(config);
    }

    public void start() {
        executor.submit(broadcaster::start);
    }

    public void stop() {
        broadcaster.stop();
        executor.shutdown();
    }

    public Node addNode() {
        if (nodeIdSequence == config.app.MAX_NODES_COUNT) {
            return null;
        }
        var node = broadcaster.addNode(nodeIdSequence);
        activeNodes.add(nodeIdSequence);
        executor.submit(node::listenMessages);
        executor.submit(node::sendMessages);
        nodeIdSequence++;

        return node;
    }

    public boolean removeNode() {
        if (activeNodes.size() <= 1) {
            return false;
        }
        int lastIdx = activeNodes.size() - 1;
        int lastNodeId = activeNodes.get(lastIdx);
        broadcaster.removeNode(lastNodeId);
        activeNodes.remove(lastIdx);
        return true;
    }

}
