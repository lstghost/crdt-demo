package io.sevenbit.crdt.demo.networking.messages;

import io.sevenbit.crdt.demo.model.Operation;

/**
 * Message with single item modification
 */
public class OperationMessage extends Message{
    public final Operation operation;

    public OperationMessage(int nodeId, Operation operation) {
        super(nodeId, MessageType.OPERATION);
        this.operation = operation;
    }

    @Override
    public String toString() {
        return "{" +
                "operation=" + operation +
                ", nodeId=" + nodeId +
                ", messageType=" + messageType +
                '}';
    }
}
