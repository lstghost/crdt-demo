package io.sevenbit.crdt.demo.networking.messages;


import io.sevenbit.crdt.demo.model.Item;

import java.util.Collection;


public class StoreStateResponseMessage extends Message {

    public final Collection<Item> storeState;

    public StoreStateResponseMessage(int nodeId, Collection<Item> storeState) {
        super(nodeId, MessageType.STORE_STATE_RESPONSE);
        this.storeState = storeState;
    }

}
