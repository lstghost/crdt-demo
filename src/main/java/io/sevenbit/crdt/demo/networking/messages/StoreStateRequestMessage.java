package io.sevenbit.crdt.demo.networking.messages;

/**
 * Message is sent by new node to copy store state from other nodes
 */
public class StoreStateRequestMessage extends Message {
    public StoreStateRequestMessage(int nodeId){
        super(nodeId, MessageType.STORE_STATE_REQUEST);
    }
}
