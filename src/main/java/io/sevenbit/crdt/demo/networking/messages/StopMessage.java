package io.sevenbit.crdt.demo.networking.messages;

/**
 * Message signals to stop consuming and producing messages
 */
public class StopMessage extends Message {
    public StopMessage() {
        super(-1, MessageType.STOP_SIGNAL);
    }
}
