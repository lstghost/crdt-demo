package io.sevenbit.crdt.demo.networking;

import io.sevenbit.crdt.demo.Config;
import io.sevenbit.crdt.demo.networking.messages.Message;
import io.sevenbit.crdt.demo.networking.messages.StopMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Broadcaster class provides an emulation of reliable message broadcasting between nodes
 * each node sends upstream operations to upstreamQueue,
 * broadcaster sends it to each node via corresponding downstream queue
 * <p>
 * Broadcaster class also manages new node creation / node removing
 */
public class Broadcaster {
    private static final Logger log = LoggerFactory.getLogger(Broadcaster.class);

    private final BlockingQueue<Message> upstreamQueue;
    private final Map<Integer, BlockingQueue<Message>> downstreamQueues;
    private final Config config;

    public Broadcaster(Config config) {
        this.config = config;
        this.upstreamQueue = new ArrayBlockingQueue<Message>(config.node.QUEUE_CAPACITY);
        this.downstreamQueues = new ConcurrentHashMap<>();
    }

    public synchronized Node addNode(int nodeId) {
        log.info("add node: {}", nodeId);
        var nodeListenQueue = new LinkedBlockingDeque<Message>(config.node.QUEUE_CAPACITY);
        var node = new Node(nodeId, config.node, upstreamQueue, nodeListenQueue);
        downstreamQueues.putIfAbsent(nodeId, nodeListenQueue);
        return node;
    }


    public synchronized void removeNode(int nodeId) {
        log.info("remove node: {}", nodeId);
        try {
            downstreamQueues.get(nodeId).put(new StopMessage());
            downstreamQueues.remove(nodeId);
        } catch (Exception ex) {
            log.error("broadcaster.removeNode exception: {}", ex.getMessage());
            ex.printStackTrace();
        }
    }

    /**
     * Start message broadcasting
     */
    public void start() {
        try {
            while (true) {
                var message = upstreamQueue.take();
                switch (message.messageType) {
                    case STOP_SIGNAL:
                        broadcast(message);
                        log.info("received stop message");
                        return;
                    case STORE_STATE_REQUEST:
                        unicast(message);
                        break;
                    default:
                        broadcast(message);
                }
            }
        } catch (Exception ex) {
            log.error("broadcaster exception: {}", ex.getMessage());
            ex.printStackTrace();
            return;
        }

    }

    private void broadcast(Message message) throws InterruptedException {
        for (var entry : downstreamQueues.entrySet()) {
            var nodeId = entry.getKey();
            var queue = entry.getValue();
//            if (nodeId != message.nodeId) { //TODO: do i need to broadcast to myself?
                queue.put(message);
//            }
        }
    }

    private void unicast(Message message) throws InterruptedException {
        var queue = downstreamQueues.entrySet().iterator().next().getValue();
        queue.put(message);

    }

    /**
     * Send stop signal to all nodes
     */
    public synchronized void stop() {
        try {
            upstreamQueue.put(new StopMessage());
        } catch (Exception ex) {
            log.error("broadcaster exception: {}", ex.getMessage());
            ex.printStackTrace();
            return;
        }
    }
}
