package io.sevenbit.crdt.demo.networking.messages;

public abstract class Message {
    public final int nodeId;
    public final MessageType messageType;

    protected Message(int nodeId, MessageType messageType) {
        this.nodeId = nodeId;
        this.messageType = messageType;
    }

    @Override
    public String toString() {
        return "Message{" +
                "nodeId=" + nodeId +
                ", messageType=" + messageType +
                '}';
    }
}
