package io.sevenbit.crdt.demo.networking.messages;

public enum MessageType {
    OPERATION,
    STOP_SIGNAL,
    STORE_STATE_REQUEST,
    STORE_STATE_RESPONSE,
}
