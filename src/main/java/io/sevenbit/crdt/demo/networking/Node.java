package io.sevenbit.crdt.demo.networking;

import io.sevenbit.crdt.demo.Config;
import io.sevenbit.crdt.demo.model.IStore;
import io.sevenbit.crdt.demo.model.Store;
import io.sevenbit.crdt.demo.monitoring.NodeMonitor;
import io.sevenbit.crdt.demo.networking.messages.Message;
import io.sevenbit.crdt.demo.networking.messages.MessageType;
import io.sevenbit.crdt.demo.networking.messages.OperationMessage;
import io.sevenbit.crdt.demo.networking.messages.StoreStateRequestMessage;
import io.sevenbit.crdt.demo.networking.messages.StoreStateResponseMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;

/**
 * Node class represents a single peer that performs collaborative modifications
 */
public class Node {
    private static final Logger log = LoggerFactory.getLogger(Node.class);
    public final NodeMonitor nodeMonitor = new NodeMonitor();

    /**
     * Unique node identifier
     */
    public final int nodeId;
    /**
     * Node will print current store state if true
     */
    public final boolean isSpeaker;

    /**
     *
     */
    private IStore localStore;

    private final Config.Node config;

    private final BlockingQueue<Message> upstreamQueue;
    private final BlockingDeque<Message> downstreamQueue;

    private volatile State state;
    //true if some other nodes asks to share store state
    private volatile boolean stateRequested = false;

    public Node(int nodeId, Config.Node config, BlockingQueue<Message> upstreamQueue, BlockingDeque<Message> downstreamQueue) {
        boolean firstNode = nodeId == 0;
        this.nodeId = nodeId;
        this.isSpeaker = firstNode;
        this.config = config;
        this.upstreamQueue = upstreamQueue;
        this.downstreamQueue = downstreamQueue;
        if (firstNode) {
            localStore = Store.fromScratch(nodeId, config.DEFAULT_STORE_SIZE);
            state = State.RUNNING;
        } else {
            localStore = Store.fromExistingStore(nodeId, config.DEFAULT_STORE_SIZE);
            state = State.WAITING_FOR_INITIAL_STATE;
        }
    }

    public void sendMessages() {
        long waitMillis = 1000 / config.NODE_OPERATIONS_PER_SECOND;
        try {
            while (true) {
                switch (state) {

                    case STOP_REQUESTED:
                        state = State.STOPPED;
                        return;

                    case WAITING_FOR_INITIAL_STATE:
                        requestStoreState();
                        Thread.sleep(2000);
                        break;

                    case RUNNING:
                        if (stateRequested) {
                            shareStoreState();
                            stateRequested = false;
                        } else {
                            sendMessage();
                        }
                        break;

                    case INIT_IN_PROGRESS:
                        Thread.sleep(1000);
                }
                Thread.sleep(waitMillis);
            }
        } catch (Exception ex) {
            log.error("node.send exception: {}", ex.getMessage());
            ex.printStackTrace();
            return;
        }
    }

    private void sendMessage() throws InterruptedException {
        var operation = localStore.randomOperation();
        var message = new OperationMessage(nodeId, operation);
        upstreamQueue.put(message);
    }

    private void requestStoreState() throws InterruptedException {
        upstreamQueue.put(new StoreStateRequestMessage(nodeId));
    }

    private void shareStoreState() throws InterruptedException {
        upstreamQueue.put(new StoreStateResponseMessage(nodeId, localStore.shareInitialState()));
    }


    public void listenMessages() {
        try {
            while (true) {
                var message = downstreamQueue.take();

                switch (state) {

                    case WAITING_FOR_INITIAL_STATE:
                        if (message.messageType == MessageType.STORE_STATE_RESPONSE) {
                            state = State.INIT_IN_PROGRESS;
                            var messageWithState = (StoreStateResponseMessage) message;
                            localStore.loadInitialState(messageWithState.storeState);
                            log.info("{}: initial state loaded", nodeId);
                            state = State.RUNNING;
                        } else if (message.messageType == MessageType.STOP_SIGNAL) {
                            this.state = State.STOP_REQUESTED;
                            return;
                        }
                        break;

                    case INIT_IN_PROGRESS:
                        downstreamQueue.addFirst(message);
                        Thread.sleep(5000);
                        break;

                    case RUNNING:
                        if (message.messageType == MessageType.OPERATION) {
                            var operationMessage = (OperationMessage) message;
                            localStore.applyOperation(operationMessage.operation);
                            nodeMonitor.operation();
                        }
                        if (message.messageType == MessageType.STOP_SIGNAL) {
                            this.state = State.STOP_REQUESTED;
                            return;
                        }
                        if (message.messageType == MessageType.STORE_STATE_REQUEST) {
                            this.stateRequested = true;
                        }
                        break;
                }

                if (isSpeaker && message.messageType != MessageType.OPERATION) {
                    log.info("{}", message);
                }

            }
        } catch (Exception ex) {
            log.error("node.listen exception: {}", ex.getMessage());
            ex.printStackTrace();
            return;
        }
    }

    public State state() {
        return state;
    }

    public boolean isSameStoreState(Node other) {
        return localStore.isSameState(other.localStore);
    }


    public enum State {
        WAITING_FOR_INITIAL_STATE,
        INIT_IN_PROGRESS,
        RUNNING,
        STOPPED,
        STOP_REQUESTED,
    }
}
