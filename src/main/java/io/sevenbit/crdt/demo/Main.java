package io.sevenbit.crdt.demo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        var config = new Config();
        var context = new Context(config);


        System.out.println("enter 'q' to terminate");
        System.out.println("enter 'a' to add node");
        System.out.println("enter 'r' to remove node");
        context.start();
        var reader = new BufferedReader(
                new InputStreamReader(System.in));

        while (true) {
            var command = reader.readLine();
            switch (command) {
                case "q" :
                    context.stop();
                    return;
                case "a":
                    context.addNode();
                    break;
                case "r":
                    context.removeNode();
                    break;
                default:
                    System.out.println("unknown command: " + command);
            }
        }

    }

}
