package io.sevenbit.crdt.demo.util;

import io.sevenbit.crdt.demo.model.Item;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Random;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;

public class StoreInitializer {
    private static final Logger log = LoggerFactory.getLogger(StoreInitializer.class);
    private final static Random r = new Random(123);
    private static final int threads = 8;
    private static final MathContext mathContext = MathContext.DECIMAL128;


    /**
     * init empty store with sorted keys from [0 to 1] and random int values
     *
     * @param id        node id
     * @param storeSize
     * @return
     */
    public static NavigableMap<BigDecimal, Item> initEmptyStore(int id, int storeSize) {
        var started = System.currentTimeMillis();
        var result = new ConcurrentSkipListMap<BigDecimal, Item>();
        result.put(BigDecimal.ZERO, Item.LEFT_BORDER); //left border

        var div = storeSize + 1;

        var step = BigDecimal.ONE.divide(BigDecimal.valueOf(div), mathContext); // step = 1 / div
        var previous = BigDecimal.ZERO;


        for (int i = 0; i < div - 1; i++) {
            var index = previous.add(step); // index = step + i * step
            previous = index;
            var value = r.nextInt();
            var item = new Item(index, value, id, BigInteger.ZERO);
            result.put(index, item);
        }
        result.put(BigDecimal.ONE, Item.RIGHT_BORDER); //right border

        long elapsed = System.currentTimeMillis() - started;
        System.out.println("init store elapsed: " + elapsed);
        return result;
    }


    /**
     * Create store with random numbers in parallel
     *
     * @param id
     * @param storeSize
     * @return
     */
    public static NavigableMap<BigDecimal, Item> initEmptyStoreParallel(int id, int storeSize) {
        try {
            if (storeSize < threads) {
                initEmptyStore(id, storeSize);
            }

            var started = System.currentTimeMillis();

            var div = storeSize + 1;
            var step = BigDecimal.ONE.divide(BigDecimal.valueOf(div), mathContext); // step = 1 / div
            var result = new ConcurrentSkipListMap<BigDecimal, Item>();

            result.put(BigDecimal.ZERO, Item.LEFT_BORDER); //left border

            var executor = Executors.newFixedThreadPool(threads);
            var latch = new CountDownLatch(threads);
            var threadsStep = div / threads;
            var prevstop = 0;
            for (int i = 0; i < threads; i++) {
                var isLast = i == threads - 1;
                var start = prevstop;
                var stop = isLast ? storeSize : start + threadsStep;

                executor.submit(() -> initStorePart(result, id,  step, start, stop, latch));
                prevstop = stop;
            }
            latch.await();

            result.put(BigDecimal.ONE, Item.RIGHT_BORDER); //right border
            long elapsed = System.currentTimeMillis() - started;
            System.out.println("init store (parallel) elapsed: " + elapsed);
            return result;
        } catch (Exception ex) {
            log.error("parallel store initialization exception: {}", ex.getMessage());
            throw new RuntimeException(ex);
        }
    }


    /**
     * Initialize part of store, intended for parallel initialization
     *
     * @param id
     * @param step
     * @param start
     * @param stop
     */
    private static void initStorePart(Map<BigDecimal, Item> data, int id, BigDecimal step, int start, int stop, CountDownLatch latch) {
        try {

            var previous = start == 0
                    ? BigDecimal.ZERO
                    : step.multiply(BigDecimal.valueOf(start));


            for (int i = start; i < stop; i++) {
                var index = previous.add(step); // index = step + i * step

                previous = index;
                var value = r.nextInt();
                var item = new Item(index, value, id, BigInteger.ZERO);
                data.put(index, item);
            }
        } finally {
            latch.countDown();
        }
    }
}
