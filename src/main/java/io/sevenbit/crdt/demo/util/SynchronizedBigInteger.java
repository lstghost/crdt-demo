package io.sevenbit.crdt.demo.util;

import java.math.BigInteger;

public final class SynchronizedBigInteger {

    private BigInteger current = BigInteger.ZERO;


    public synchronized BigInteger incrementAndGet() {
        current = current.add(BigInteger.ONE);
        return current;
    }

    public synchronized BigInteger setValueIfGreaterAndIncrement(BigInteger value) {
        if (current.compareTo(value) < 0) {
            current = value;
        }
        current.add(BigInteger.ONE);
        return current;

    }
}
