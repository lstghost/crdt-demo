# Description 
It is a test implementation of an operation-based CRDT algorithm. We have a cluster with multiple nodes. New nodes can join the cluster, the old ones can disconnects. 
Each node has a local version of the ordered collection of items. The node generates item modifications. One modification adds, updates, or deletes an item. Modifications should be replicated to other nodes without conflicts. So we assume that after a bunch of concurrent modifications all nodes should eventually have the same store state.

### Network model
Nodes broadcast messages to all other nodes. We can choose between casual broadcast and FIFO total order broadcast. Each node is equal to another, so we don't have a master node. The first node is a special one only in a way to initialize the storage: we generate a new store from scratch for the first node, all other nodes copy state from the existing node. 

### Store model
The ordered collection of items is implemented with the SkipList data structure. Key is a rational number between 0 and 1. When we want to add a new item between existing ones we generate an index value as follows: ```new_index = (previous_index + next_index) / 2```

Item has the following structure:
````
Item {
    index:      BigDecimal
    value:      int
    timestamp:  BigInteger
    nodeId:     int
}           
````
```index``` is equal to the key in the SkipList. We can determine an item's position by comparing these indexes. 

```value``` is an arbitrary integer value. There are no limitations on the value field, we can choose another type if needed. 

```timestamp``` is equal to Lamport clock time at the moment when modification happened. The default value is 0. 

```nodeId``` is equal to the node identifier that generated the modification.

There is also an auxiliary hash map to handle tombstones.   


## Conflict resolution
When a new modification arrives in the local store, firstly we are checking if an item with the same key already exists either in the SkipList store or in the tombstones store. If it exists, we are comparing the Lamport clock timestamp and keeping the item with the greater timestamp. In case if timestamps are equal we use nodeId as a tiebreaker. If it not exists we just modify the item (either in SkipList store or in tombstone store depending on the operation)

Lamport clock is incremented on every operation creation. When new operration applied clock is set to ```max(currentClock, itemClock) + 1```
## Implementation details
### Node states
```
WAITING_FOR_INITIAL_STATE,
INIT_IN_PROGRESS,
RUNNING,
STOP_REQUESTED
STOPPED
```
### Threads
The main app thread listens to user input and starts/stops all other threads.  Each node occupies two threads: one thread for upstream messages and one for downstream.

Broadcaster thread listens to upstream messages from each node and downstream it to all other nodes. All inter-thread communication is done via queues

### node initialization
The first node inits itself with random int numbers. Other nodes request the current state with special messages broadcasted to others.
### node stopping
When the node receive stop messages it sets node state to STOP_REQUESTED and stops listening thread;
When node's message spawning thread reads STOP_REQUESTED state it stops execution;



## Memory usage estimation
For each Item: 
```
value:      4 bytes
index:      2x4 bytes
timestamp:  4 bytes
nodeId:     4 bytes

sizeof(item) = 4 + 8 + 4 + 4 = 20 bytes
sizeof(key) =  4bytes 

sizeof(store) = sizeof(item) * 10M = (20 bytes + 4bytes)* 10M = 240 Megabytes

```
So for 20 nodes all local stores in total takes up to ```20 * 240Megabytes ~ 5GB```

## Algorithm complexity
I choose SkipList data structure because it gives us ```O(log(n))``` complexity in average for all needed operations (search/insert/delete). 
### Issues
* Initializing a skip list with 10_000_000 items is a slow operation. It takes up to 6 seconds to perform initialization. 
* Memory is the bottleneck, GC takes significant time with large store sizes and nodes count ~ 20. 


### Current tasks
* Global pause/resume API to verify the current state of the nodes
* Add shuffle to implement casual broadcasting
* Monitor op/sec speed
* Make project easy to demonstrate
* Random operations timings
